const Dashboard = artifacts.require("Dashboard");
const DashboardLibrary = artifacts.require("DashboardLibrary");

module.exports = function (deployer) {
  deployer.link(DashboardLibrary, Dashboard);
  deployer.deploy(Dashboard);
};
