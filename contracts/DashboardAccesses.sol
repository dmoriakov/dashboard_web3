// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./DashboardLibrary.sol";

contract DashboardAccesses is Ownable {
    using DashboardLibrary for string;

    string[] roles;
    mapping(string => string[]) roleToAccesses;
    mapping(address => string[]) addressToRoles;

    function addRole(string calldata _role) external onlyOwner {
        require(!_role.isElementOf(roles), "Role already exists");
        roles.push(_role);
    }

    function removeRole(string calldata _role) external onlyOwner {
        require(_role.isElementOf(roles), "Role does not exist");
        _role.deleteFrom(roles);
        delete roleToAccesses[_role];
    }

    function addAccess(string calldata _role, string calldata _access)
        external
        onlyOwner
    {
        require(_role.isElementOf(roles), "Role does not exist");
        require(
            !_access.isElementOf(roleToAccesses[_role]),
            "Access already granted"
        );
        roleToAccesses[_role].push(_access);
    }

    function removeAccess(string calldata _role, string calldata _access)
        external
        onlyOwner
    {
        require(_role.isElementOf(roles), "Role does not exist");
        require(
            _access.isElementOf(roleToAccesses[_role]),
            "Role does not have this access"
        );
        _role.deleteFrom(roleToAccesses[_role]);
    }

    function reassignRolesForUser(
        address _targetAddress,
        string[] memory _roles
    ) external onlyOwner {
        for (uint256 i; i < _roles.length; i++) {
            require(
                _roles[i].isElementOf(_roles),
                "Some of provided roles do not exist"
            );
        }
        addressToRoles[_targetAddress] = _roles;
    }

    function reassignAccessesForRole(
        string calldata _role,
        string[] memory _accesses
    ) external onlyOwner {
        require(_role.isElementOf(roles), "Role does not exist");
        roleToAccesses[_role] = _accesses;
    }

    function getRoles() external view returns (string[] memory) {
        return roles;
    }

    function getRolesForUser(address _targetAddress)
        public
        view
        returns (string[] memory)
    {
        return addressToRoles[_targetAddress];
    }

    function getAccessesForRole(string memory _role)
        public
        view
        returns (string[] memory)
    {
        return roleToAccesses[_role];
    }

    function hasAccess(address _targetAddress, string memory _access)
        internal
        view
        returns (bool)
    {
        string[] memory _roles = getRolesForUser(_targetAddress);
        string[] memory _accesses;
        for (uint256 i = 0; i < _roles.length; i++) {
            _accesses = getAccessesForRole(_roles[i]);
            if (_access.isElementOf(_accesses)) {
                return true;
            }
        }
        return false;
    }
}
