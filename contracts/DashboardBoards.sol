// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./DashboardAccesses.sol";
import "./DashboardLibrary.sol";

contract DashboardBoards is DashboardAccesses {
    using DashboardLibrary for string;
    string[] boards;

    function addBoard(string calldata _name) external onlyOwner {
        require(!_name.isElementOf(boards), "Board already exists");
        boards.push(_name);
    }

    function removeBoard(string calldata _name) external onlyOwner {
        require(_name.isElementOf(boards), "Board does not exist");
        _name.deleteFrom(boards);
    }

    function getBoards() external view returns (string[] memory) {
        return boards;
    }
}
