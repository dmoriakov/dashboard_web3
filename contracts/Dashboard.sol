// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./DashboardBoards.sol";
import "./DashboardLibrary.sol";

contract Dashboard is DashboardBoards {
    using DashboardLibrary for string;
    struct Task {
        uint256 id;
        string name;
        string description;
        string board;
        address createdBy;
        address[] assignedTo;
    }

    event NewTask(uint256 id);
    event TaskDeleted(uint256 id);
    event TaskChanged(uint256 id);
    error InvalidTaskId(uint256 id);

    Task[] tasks;
    uint256 latestId;

    function getTasks() external view returns (Task[] memory) {
        return tasks;
    }

    function getTaskById(uint256 _id)
        public
        view
        returns (Task memory, uint256)
    {
        for (uint256 i = 0; i < tasks.length; i++) {
            if (tasks[i].id == _id) {
                return (tasks[i], i);
            }
        }
        revert InvalidTaskId(_id);
    }

    function addTask(
        string calldata _name,
        string calldata _description,
        string calldata _board,
        address[] calldata _assignedTo
    ) external {
        require(_board.isElementOf(boards), "No such board");
        require(hasAccess(msg.sender, _board), "No access");
        latestId++;
        tasks.push(
            Task(latestId, _name, _description, _board, msg.sender, _assignedTo)
        );
        emit NewTask(latestId);
    }

    function removeTask(uint256 _id) external {
        (, uint256 _arrIndex) = getTaskById(_id);
        string memory board = tasks[_arrIndex].board;
        require(hasAccess(msg.sender, board), "No access");
        _deleteTask(_arrIndex);
        emit TaskDeleted(_id);
    }

    function editTask(
        uint256 _id,
        string calldata _name,
        string calldata _description,
        string calldata _board,
        address[] calldata _assignedTo
    ) external {
        (, uint256 _arrIndex) = getTaskById(_id);
        require(hasAccess(msg.sender, tasks[_arrIndex].board), "No access");
        tasks[_arrIndex] = Task(
            _id,
            _name,
            _description,
            _board,
            tasks[_arrIndex].createdBy,
            _assignedTo
        );
        emit TaskChanged(_id);
    }

    function _deleteTask(uint256 _arrIndex) private {
        tasks[_arrIndex] = tasks[tasks.length - 1];
        tasks.pop();
    }
}
