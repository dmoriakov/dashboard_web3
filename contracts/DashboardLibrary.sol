// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

library DashboardLibrary {
    function isElementOf(string memory self, string[] memory arr)
        public
        pure
        returns (bool)
    {
        for (uint256 i = 0; i < arr.length; i++) {
            if (
                keccak256(abi.encodePacked(arr[i])) ==
                keccak256(abi.encodePacked(self))
            ) {
                return true;
            }
        }
        return false;
    }

    function deleteFrom(string memory self, string[] storage arr) public {
        uint256 index;
        for (uint256 i = 0; i < arr.length; i++) {
            if (
                keccak256(abi.encodePacked(arr[i])) ==
                keccak256(abi.encodePacked(self))
            ) {
                index = i;
            }
        }
        arr[index] = arr[arr.length - 1];
        arr.pop();
    }
}
