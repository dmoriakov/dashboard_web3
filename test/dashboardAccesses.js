const Dashboard = artifacts.require("Dashboard");
const DashboardLibrary = artifacts.require("DashboardLibrary");

contract("DashboardAccesses", async (accounts) => {
  let contract;

  describe("Roles", () => {
    beforeEach(async () => {
      await DashboardLibrary.new();
      contract = await Dashboard.new();
    });

    it("should add role", async () => {
      const [alice] = accounts;
      await contract.addRole("admin", { from: alice });
      await contract.addRole("moderator", { from: alice });
      const roles = await contract.getRoles();
      expect(roles).deep.to.equal(["admin", "moderator"]);
    });

    it("should remove role", async () => {
      const [alice] = accounts;
      await contract.addRole("admin", { from: alice });
      await contract.addRole("moderator", { from: alice });
      await contract.removeRole("admin", { from: alice });
      const roles = await contract.getRoles();
      expect(roles).deep.to.equal(["moderator"]);
    });

    it("should reassign roles for user", async () => {
      const [alice] = accounts;
      await contract.addRole("admin", { from: alice });
      await contract.addRole("moderator", { from: alice });
      await contract.reassignRolesForUser(alice, ["admin"], { from: alice });
      const roles = await contract.getRolesForUser(alice);
      expect(roles).deep.to.equal(["admin"]);
    });
  });

  describe("Accesses", () => {
    beforeEach(async () => {
      await DashboardLibrary.new();
      contract = await Dashboard.new();
    });

    it("should add access", async () => {
      const [alice] = accounts;
      await contract.addRole("admin", { from: alice });
      await contract.addAccess("admin", "access1");
      await contract.addAccess("admin", "access2");
      const accessesAdmin = await contract.getAccessesForRole("admin");
      expect(accessesAdmin).deep.to.equal(["access1", "access2"]);
    });

    it("should remove access", async () => {
      const [alice] = accounts;
      await contract.addRole("admin", { from: alice });
      await contract.addAccess("admin", "access1");
      await contract.addAccess("admin", "access2");
      await contract.removeAccess("admin", "access1");
      const accessesAdmin = await contract.getAccessesForRole("admin");
      expect(accessesAdmin).deep.to.equal(["access2"]);
    });

    it("should reassign accesses for role", async () => {
      const [alice] = accounts;
      await contract.addRole("admin", { from: alice });
      await contract.addRole("moderator", { from: alice });
      await contract.reassignAccessesForRole("admin", ["access1", "access2"]);
      let accessesAdmin = await contract.getAccessesForRole("admin");
      const accessesModerator = await contract.getAccessesForRole("moderator");
      expect(accessesAdmin).deep.to.equal(["access1", "access2"]);
      expect(accessesModerator).deep.to.equal([]);
      await contract.reassignAccessesForRole("admin", ["access2"]);
      accessesAdmin = await contract.getAccessesForRole("admin");
      expect(accessesAdmin).deep.to.equal(["access2"]);
    });
  });
});
